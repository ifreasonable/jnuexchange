package com.jnu.yishu.common;

import com.alibaba.fastjson.JSONObject;


public class ApiException extends RuntimeException{
    CodeMsg codeMsg;

    public ApiException(CodeMsg codeMsg) {
        super(codeMsg.getMsg());
        this.codeMsg = codeMsg;
    }

    public String getMsg() {
        return JSONObject.toJSONString(codeMsg);
    }
}
