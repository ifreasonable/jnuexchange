package com.jnu.yishu.common;


import com.alibaba.fastjson.JSONObject;

public class CodeMsg {

    private int code;//错误码

    private String msg;//错误信息

    /**
     * 构造函数私有化即单例模式
     * @param code 错误码
     * @param msg  错误提示
     */
    private CodeMsg(int code,String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String toJson() {
        return JSONObject.toJSONString(this);
    }


    // 处理成功消息码
    public static CodeMsg SUCCESS = new CodeMsg(0, "success");

    // 通用替补错误码，不建议用
    @Deprecated
    public static CodeMsg ERROR = new CodeMsg(-1, "fail");

    // 授权认证类错误
    public static CodeMsg TOKEN_EXPIRE_ERROR = new CodeMsg(-2001, "Token逾期失效");
    public static CodeMsg NOT_LOGIN_ERROR = new CodeMsg(-2002, "未登录");
    public static CodeMsg SIGNUP_DATA_ERROR = new CodeMsg(-2003, "注册信息错误");
    public static CodeMsg OCCUPIED_USERNAME_ERROR = new CodeMsg(-2004, "用户名已被使用");
    public static CodeMsg USER_NOT_EXIST = new CodeMsg(-2005, "用户不存在");
    public static CodeMsg LOGIN_PASSWORD_ERROR = new CodeMsg(-2006, "密码错误");
    public static CodeMsg LOGIN_SERVICE_ERROR = new CodeMsg(-2007, "登录异常");
    public static CodeMsg UNAUTHORIZED_OPERATION_ERROR = new CodeMsg(-2008, "未授权的操作");

    public static CodeMsg MSG_NOT_READABLE_ERROR = new CodeMsg(-3001, "请求体json格式错误");

    public static CodeMsg invalidProperty(String msg) {
        return new CodeMsg(-3002, msg);
    }

    // basic CRUD
    public static CodeMsg NOT_A_TIME_PERIOD_ERROR = new CodeMsg(-4001, "提交的时间戳必须构成非空集合");
    public static CodeMsg OBJECT_NOT_FOUND_ERROR = new CodeMsg(-4002, "找不到对象");
    public static CodeMsg ID_NOT_FOUND_ERROR = new CodeMsg(-4003, "未传入对象id");
    public static CodeMsg CONTENT_IS_EMPTY_ERROR = new CodeMsg(-4004, "内容不能为空");


    public static CodeMsg UPLOAD_FAILED_ERROR = new CodeMsg(-5001, "上传失败");

}