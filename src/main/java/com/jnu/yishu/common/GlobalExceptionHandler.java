package com.jnu.yishu.common;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

@RestControllerAdvice
public class GlobalExceptionHandler {
    Logger logger = Logger.getLogger(GlobalExceptionHandler.class.getName());

    @ExceptionHandler(value = IllegalArgumentException.class)
    public ResponseEntity<String> parameterErrorHandler(HttpServletRequest req, IllegalArgumentException e) {
        logger.log(Level.WARNING,"api error", e);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    // 在Controller中使用@Validated验证抛出的异常
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<String> methodArgumentErrorHandler(MethodArgumentNotValidException e) {
        logger.log(Level.WARNING,"api error", e);
        FieldError fieldError = e.getBindingResult().getFieldError();
        String msg = fieldError != null ? fieldError.getDefaultMessage() : "";
        CodeMsg codeMsg = CodeMsg.invalidProperty(msg);
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8");
        return new ResponseEntity<>(codeMsg.toJson(), headers, HttpStatus.BAD_REQUEST);
    }

    // 在Entity中（无groups？）参数验证抛出的异常
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<String> constraintViolationErrorHandler(ConstraintViolationException e) {
        logger.log(Level.WARNING,"api error", e);

        Set<ConstraintViolation<?>> violations = e.getConstraintViolations();
//        String propertyName = violations.iterator().next().getPropertyPath().toString();
        String msg = violations.iterator().next().getMessage();
        CodeMsg codeMsg = CodeMsg.invalidProperty(msg);
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8");
        return new ResponseEntity<>(codeMsg.toJson(), headers, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<String> httpMsgNotReadableErrorHandler(HttpMessageNotReadableException e) {
        logger.log(Level.WARNING,"api error", e);
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8");
        return new ResponseEntity<>(CodeMsg.MSG_NOT_READABLE_ERROR.toJson(), headers, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ApiException.class)
    public ResponseEntity<String> apiExceptionHandler(ApiException e) {
        logger.log(Level.WARNING,"api error", e);
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8");
        return new ResponseEntity<>(e.getMsg(), headers, HttpStatus.BAD_REQUEST);
    }

    // TODO: 不支持的访问方式 HttpRequestMethodNotSupportedException

    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public void othersErrorHandler(HttpServletRequest req, Exception e){
        String url = req.getRequestURI();
        logger.log(Level.SEVERE, "request error at " + url, e);
    }
}
