package com.jnu.yishu.common;


/**
 * 统一返回对象
 */

@Deprecated
public class Result<T> {

    private int code;//错误码

    private String msg;//返回的具体信息

    private T data; //定义返回的数据

    private Result(){}  //构造函数私有化,不允许任意创建对象

    private Result(CodeMsg codeMsg){
        if(codeMsg != null){
            this.code = codeMsg.getCode();
            this.msg = codeMsg.getMsg();
        }
    }

    private Result(T data,CodeMsg codeMsg){
        if(codeMsg != null){
            this.code = codeMsg.getCode();
            this.msg = codeMsg.getMsg();
        }
        this.data = data;
    }

    private Result(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    private Result(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static <T>Result<T> success(T data){
        return new Result<>(data, CodeMsg.SUCCESS);
    }

    public static <T>Result<T> error(CodeMsg codeMsg){
        return new Result<>(codeMsg);
    }
    public static <T>Result<T> error(Integer code, String msg, T data){
        return new Result<>(code, msg, data);
    }
    public static <T>Result<T> error(Integer code, String msg){
        return new Result<>(code, msg);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


}