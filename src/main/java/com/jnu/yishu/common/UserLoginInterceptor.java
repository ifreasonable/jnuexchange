package com.jnu.yishu.common;


import com.jnu.yishu.common.util.JWTUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

public class UserLoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // http的header中获得token
        String token = request.getHeader(JWTUtils.TOKEN);

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json;charset=UTF-8");
//        response.setHeader("Access-Control-Allow-Origin", "*");
//        response.setHeader("Access-Control-Allow-Headers","*");
//        response.setHeader("Access-Control-Allow-Methods","*");
//        response.setHeader("Access-Control-Allow-Credentials","true");
//        response.setHeader("Access-Control-Max-Age","3600");

        // token不存在
        if (token == null || token.equals("")) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            PrintWriter pw = response.getWriter();
            pw.write(CodeMsg.NOT_LOGIN_ERROR.toJson());
            pw.flush();
            pw.close();
            return false;
        }

        // 验证token
        String sub = JWTUtils.validateToken(token);
        if (sub == null || sub.equals("")) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            PrintWriter pw = response.getWriter();
            pw.write(CodeMsg.TOKEN_EXPIRE_ERROR.toJson());
            pw.flush();
            pw.close();
            return false;
        }

        return true;
    }
}