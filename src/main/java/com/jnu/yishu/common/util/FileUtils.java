package com.jnu.yishu.common.util;


import java.io.File;
import java.io.FileOutputStream;
import java.util.UUID;


public class FileUtils {

    /**
     * 保存文件
     * @param file 文件的byte数组
     * @param filePath 保存路径
     * @param fileName 文件名
     */
    public static void saveFile(byte[] file, String filePath, String fileName) throws Exception {
        File targetFile = new File(filePath);
        if (!targetFile.exists()) {
            targetFile.mkdirs();
        }
        FileOutputStream out = new FileOutputStream(filePath + fileName);
        out.write(file);
        out.flush();
        out.close();
    }

    /**
     * 删除文件
     * @param filePath 本地文件路径（含文件名）
     */
    public static boolean deleteFile(String filePath) {
        File targetFile = new File(filePath);
        return targetFile.delete();
    }


    /**
     * 将文件重命名为UUID
     * @param fileName 文件名
     * @return 重命名后的文件名
     */
    public static String renameToUUID(String fileName) {
        return UUID.randomUUID() + "." + fileName.substring(fileName.lastIndexOf(".") + 1);
    }

}