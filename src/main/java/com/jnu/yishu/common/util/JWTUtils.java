package com.jnu.yishu.common.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Date;


@Component
@ConfigurationProperties(prefix = "jwt")
public class JWTUtils {

    //定义token返回头部
    public static String header;

    //token前缀
    public static String tokenPrefix;

    //签名密钥
    public static String secret;

    //有效期
    public static long expireTime;

    //存进客户端的token的key名
    public static final String TOKEN = "token";

    public void setHeader(String header) {
        JWTUtils.header = header;
    }

    public void setTokenPrefix(String tokenPrefix) {
        JWTUtils.tokenPrefix = tokenPrefix;
    }

    public void setSecret(String secret) {
        JWTUtils.secret = secret;
    }

    public void setExpireTime(int expireTimeInt) {
        JWTUtils.expireTime = expireTimeInt*1000L;
    }

    /**
     * 创建TOKEN
     * @param sub token对应用户
     * @return token
     */
    public static String createToken(String sub){
        return tokenPrefix + JWT.create()
                .withSubject(sub)
                .withExpiresAt(new Date(System.currentTimeMillis() + expireTime))
                .sign(Algorithm.HMAC512(secret));
    }

    /**
     * 创建TOKEN
     * @param uid token对应用户
     * @return token
     */
    public static String createToken(Long uid){
        return createToken(String.valueOf(uid));
    }

    /**
     * 验证token
     * @param token 要验证的token
     * @return token对应用户，若验证失败则返回空字符
     */
    public static String validateToken(String token){
        try {
            return JWT.require(Algorithm.HMAC512(secret))
                    .build()
                    .verify(token.replace(tokenPrefix, ""))
                    .getSubject();
        } catch (Exception e){
            return "";
        }
    }

    /**
     * 验证token，并核对uid
     * @param token 要验证的token
     * @param uid 要验证的uid
     * @return 是否一致
     */
    public static Boolean validateToken(String token, Long uid){
        return validateToken(token).equals(String.valueOf(uid));
    }

    /**
     * 检查token是否需要更新
     * @param token 要验证的token
     * @return 是否需要更新
     */
    public static boolean isNeedUpdate(String token){
        //获取token过期时间
        Date expiresAt;
        try {
            expiresAt = JWT.require(Algorithm.HMAC512(secret))
                    .build()
                    .verify(token.replace(tokenPrefix, ""))
                    .getExpiresAt();
        } catch (TokenExpiredException e){
            return true;
        } catch (Exception e){
            return false;
        }
        //如果剩余过期时间少于过期时长的一般时 需要更新
        return (expiresAt.getTime()-System.currentTimeMillis()) < (expireTime>>1);
    }
}