package com.jnu.yishu.common.util.validator;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.jnu.yishu.common.util.validator.impl.AtLeastOneNotEmptyValidator;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * The annotated class must not have specified fields be {@code null} simultaneously.
 * Utilizes Reflex to read object field. Recognizes empty string as {@code null}.
 * 
 * @see https://www.jianshu.com/p/4bddeae22f10
 */
@Target( { TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = AtLeastOneNotEmptyValidator.class)
@Documented
public @interface AtLeastOneNotEmpty {
    String message() default "至少有一个属性不可为空";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String[] fields();
}
