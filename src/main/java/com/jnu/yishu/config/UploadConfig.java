package com.jnu.yishu.config;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 文件上传路径配置
 */
@Component
@ConfigurationProperties(prefix = "upload")
public class UploadConfig {

    // 保存路径
    private String savePath;

    // 访问路径
    private String requestPath;

    public String getSavePath() {
        return savePath;
    }

    public void setSavePath(String savePath) {
        this.savePath = savePath.replace(".", System.getProperty("user.dir"));      // 将相对路径转为绝对路径
    }

    public String getRequestPath() {
        return requestPath;
    }

    public void setRequestPath(String requestPath) {
        this.requestPath = requestPath;
    }
}
