package com.jnu.yishu.controller;

import com.jnu.yishu.common.ApiException;
import com.jnu.yishu.common.CodeMsg;
import com.jnu.yishu.common.util.JWTUtils;
import com.jnu.yishu.entity.Bulletin;
import com.jnu.yishu.service.BulletinService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

// TODO 参数校验

@Api(tags = {"公告接口"})
@RestController
public class BulletinController {
    @Autowired
    BulletinService bulletinService;

    // 以防止重复构造
    Timestamp gTimestamp = new Timestamp(System.currentTimeMillis());

    @ApiOperation("获取公告列表（有分页）")
    @GetMapping("/bulletin")
    public List getBulletins(
            @ApiParam(defaultValue = "1") @PathParam("currentPage") Integer currentPage,
            @ApiParam(defaultValue = "10") @PathParam("pageSize") Integer pageSize,
            @ApiParam(value = "按该属性排序。可以传多个。") @PathParam("prop") ArrayList<String> prop,
            @ApiParam(value = "排序方式。（0为降序，1为升序）可以传多个。\n如果列表为空时加入该参数会收到bad request") @PathParam("sort") ArrayList<Integer> sort) {
        Pageable pageable;
        ArrayList<Sort.Order> orders = new ArrayList<>();

        if (currentPage == null) currentPage = 1;
        if (pageSize == null) pageSize = 10;

        if (prop.size() == 0) {
            pageable = PageRequest.of(currentPage, pageSize);
        } else {
            for (int i = 0; i < prop.size(); i += 1) {
                if (sort.size() > i && sort.get(i) == 1)
                    orders.add(new Sort.Order(Sort.Direction.ASC, prop.get(i)));
                else
                    orders.add(new Sort.Order(Sort.Direction.DESC, prop.get(i)));
            }
            pageable = PageRequest.of(currentPage, pageSize, Sort.by(orders));
        }

        return bulletinService.getAllBulletin(pageable).getContent();
    }

    @ApiOperation("获取单个公告")
    @GetMapping("/bulletin/{id}")
    public Bulletin getBulletinDetail(@PathVariable("id") Long id) {
        checkId(id);
        return bulletinService.getBulletin(id);
    }

    @ApiOperation("获取公告总数量")
    @GetMapping("/bulletin/count")
    public Long getBulletinCount() {
        return bulletinService.getBulletinCount();
    }

    @ApiOperation("获取当前在生效期间的公告总数量")
    @GetMapping("/bulletin/alive/count")
    public Long getAliveBulletinCount() {
        gTimestamp.setTime(System.currentTimeMillis());
        return bulletinService.getAliveBulletinCount(gTimestamp);
    }

    @ApiOperation("获取当前在生效期间的公告列表")
    @GetMapping("/bulletin/alive")
    public List getAliveBulletins() {
        gTimestamp.setTime(System.currentTimeMillis());
        return bulletinService.getAliveBulletin(gTimestamp);
    }

    @ApiOperation(value = "新建公告", notes = "json可以不包含该实体的id，即使包含，也将被忽略")
    @PostMapping("/bulletin")
    public Bulletin newBulletin(
            @RequestHeader(value = JWTUtils.TOKEN, required = false) String token,
            @Validated({Bulletin.ValidNewBulletin.class}) @RequestBody Bulletin bulletin) {
        checkUser(token);
        checkTimestamp(bulletin);
        //前端不能维护id,所以不如直接忽略。而且jpa要求为null才认为是新的。
        bulletin.setBulletinId(null);
        return bulletinService.insertBulletin(bulletin);
    }

    @ApiOperation("修改公告")
    @PutMapping("/bulletin")
    public Bulletin alterBulletin(
            @RequestHeader(value = JWTUtils.TOKEN, required = false) String token,
            @Validated({Bulletin.ValidAlterBulletin.class}) @RequestBody Bulletin bulletin) {
        checkUser(token);
        checkId(bulletin.getBulletinId());
        checkTimestamp(bulletin);
        return bulletinService.updateBulletin(bulletin);
    }

    @ApiOperation("删除公告")
    @DeleteMapping("/bulletin/{id}")
    public void removeBulletin(
            @RequestHeader(value = JWTUtils.TOKEN, required = false) String token,
            @PathVariable("id") Long id) {
        checkUser(token);
        checkId(id);
        bulletinService.deleteBulletin(id);
    }

    /**
     * Check if token exists. If not, throw ApiException.
     * If yes, check if it is valid. If not valid, throw ApiException.
     */
    private void checkUser(String token) {
        if (token == null) throw new ApiException(CodeMsg.NOT_LOGIN_ERROR);
        if (JWTUtils.validateToken(token).equals(""))
            throw new ApiException(CodeMsg.TOKEN_EXPIRE_ERROR);
    }

    /**
     * Check if this id exists. If not, throw ApiException.
     */
    private void checkId(Long id) {
        if (!bulletinService.isBulletinExists(id))
            throw new ApiException(CodeMsg.OBJECT_NOT_FOUND_ERROR);
    }

    /**
     * Check if the timestamp make sense. If not, throw ApiException.
     */
    private void checkTimestamp(Bulletin bulletin) {
        if (bulletin.getExpireTime() != null && bulletin.getAnnounceTime() != null) {
            if (bulletin.getAnnounceTime().compareTo(bulletin.getExpireTime()) > 0)
                throw new ApiException(CodeMsg.NOT_A_TIME_PERIOD_ERROR);
        }
    }
}
