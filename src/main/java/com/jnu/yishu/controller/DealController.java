package com.jnu.yishu.controller;

import com.jnu.yishu.common.ApiException;
import com.jnu.yishu.common.CodeMsg;
import com.jnu.yishu.common.util.JWTUtils;
import com.jnu.yishu.entity.Deal;
import com.jnu.yishu.service.DealService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;

// TODO 参数校验 如userId和bookId

@Api(tags = {"交易接口"})
@RestController
public class DealController {
    @Autowired
    DealService dealService;

    @ApiOperation("获取交易列表（有分页）")
    @GetMapping("/deal")
    public List getDeals(
            @ApiParam(defaultValue = "1") @PathParam("currentPage") Integer currentPage,
            @ApiParam(defaultValue = "10") @PathParam("pageSize") Integer pageSize,
            @ApiParam(value = "按该属性排序。可以传多个。") @PathParam("prop") ArrayList<String> prop,
            @ApiParam(value = "排序方式。（0为降序，1为升序）可以传多个。\n如果列表为空时加入该参数会收到bad request") @PathParam("sort") ArrayList<Integer> sort) {
        Pageable pageable;
        ArrayList<Sort.Order> orders = new ArrayList<>();

        if (currentPage == null) currentPage = 1;
        if (pageSize == null) pageSize = 10;

        if (prop.size() == 0) {
            pageable = PageRequest.of(currentPage, pageSize);
        } else {
            for (int i = 0; i < prop.size(); i += 1) {
                if (sort.size() > i && sort.get(i) == 1)
                    orders.add(new Sort.Order(Sort.Direction.ASC, prop.get(i)));
                else
                    orders.add(new Sort.Order(Sort.Direction.DESC, prop.get(i)));
            }
            pageable = PageRequest.of(currentPage, pageSize, Sort.by(orders));
        }

        return dealService.getAllDeal(pageable).getContent();
    }

    @ApiOperation("获取单个交易")
    @GetMapping("/deal/{id}")
    public Deal getDealDetail(@PathVariable("id") Long id) {
        checkId(id);
        return dealService.getDeal(id);
    }

    @ApiOperation("获取交易总数量")
    @GetMapping("/deal/count")
    public Long getDealCount() {
        return dealService.getDealCount();
    }


    @ApiOperation(value = "新建交易", notes = "json可以不包含该实体的id，即使包含，也将被忽略")
    @PostMapping("/deal")
    public Deal newDeal(
            @RequestHeader(value = "token", required = false) String token,
            @Validated({Deal.ValidNewDeal.class}) @RequestBody Deal deal) {
        //checkUser(token);
        //前端不能维护id,所以不如直接忽略。而且jpa要求为null才认为是新的。
        deal.setDealId(null);
        return dealService.insertDeal(deal);
    }

    @ApiOperation("修改交易")
    @PutMapping("/deal")
    public Deal alterDeal(
            @RequestHeader(value = "token", required = false) String token,
            @Validated({Deal.ValidAlterDeal.class}) @RequestBody Deal deal) {
        checkUser(token);
        return dealService.updateDeal(deal);
    }

    @ApiOperation("删除交易")
    @DeleteMapping("/deal/{id}")
    public void removeDeal(
            @RequestHeader(value = "token", required = false) String token,
            @PathVariable("id") Long id) {
        checkUser(token);
        checkId(id);
        dealService.deleteDeal(id);
    }

    /**
     * Check if token exists. If not, throw ApiException.
     * If yes, check if it is valid. If not valid, throw ApiException.
     */
    private void checkUser(String token) {
        if (token == null) throw new ApiException(CodeMsg.NOT_LOGIN_ERROR);
        if (JWTUtils.validateToken(token).equals(""))
            throw new ApiException(CodeMsg.TOKEN_EXPIRE_ERROR);
    }

    /**
     * Check if this id exists. If not, throw ApiException.
     */
    private void checkId(Long id) {
        if (!dealService.isDealExists(id))
            throw new ApiException(CodeMsg.OBJECT_NOT_FOUND_ERROR);
    }
}
