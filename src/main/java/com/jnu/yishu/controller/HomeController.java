package com.jnu.yishu.controller;

import com.jnu.yishu.common.ApiException;
import com.jnu.yishu.common.CodeMsg;
import com.jnu.yishu.common.util.JWTUtils;
import com.jnu.yishu.entity.User;
import com.jnu.yishu.service.FileService;
import com.jnu.yishu.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;


@Api(tags = {"首页接口"})
@RestController
@RequestMapping("/")
@Validated
public class HomeController {
    @Autowired
    UserService userService;
    @Autowired
    FileService fileService;

    @ApiOperation(value = "用户注册", notes = "至少需要用户名和密码，会忽略管理权限、认证状态、信用状态、uid字段")
    @ApiResponses({
            @ApiResponse(code = 200, message = "注册成功，返回用户信息"),
            @ApiResponse(code = 400, message = "注册失败，返回具体错误信息", response = CodeMsg.class)
    })
    @PostMapping("/signup")
    public User signup(@Validated(User.ValidSignup.class) @RequestBody User user) {
        return userService.signup(user).noPw();
    }

    @ApiOperation(value = "用户登录", notes = "仅需要用户名与密码，返回header中存放token，body为用户信息")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登录成功，返回{'token': 'string'}"),
            @ApiResponse(code = 400, message = "登录失败，返回具体错误信息", response = CodeMsg.class)
    })
    @PostMapping("/login")
    public User login(@Validated({User.ValidLogin.class}) @RequestBody User user, HttpServletResponse response) {
        response.setHeader(JWTUtils.TOKEN, userService.login(user));
        return userService.findByUsername(user.getUsername()).noPw();
    }

    @ApiOperation(value = "注销登录", notes = "后端不对token进行处理，前端需清除token")
    @PostMapping("/logout")
    public Map<String, String> logout(@RequestParam(value = "uid") Long uid) {
        Map<String, String> map = new HashMap<>();
        map.put("data", "success");
        return map;
    }


    @ApiOperation(value = "上传文件", notes = "header中需有与uid一致的token")
    @ApiResponses({
            @ApiResponse(code = 200, message = "上传成功，返回文件路径{'path': 'string'}"),
            @ApiResponse(code = 400, message = "上传失败，返回具体错误信息", response = CodeMsg.class)
    })
    @PostMapping("/file")
    public Map<String, String> uploadFile(@RequestParam(value = "file") MultipartFile file,
                                          @RequestParam(value = "uid") Long uid,
                                          @RequestHeader(value = JWTUtils.TOKEN) String token) {
        checkToken(token, uid);
        Map<String, String> map = new HashMap<>();
        map.put("path", fileService.uploadFile(file, uid));
        return map;
    }

    /**
     * Check if token exists. If not, throw ApiException.
     * If yes, check if it is valid and corresponding with uid. If not valid, throw ApiException.
     */
    private void checkToken(String token, Long uid) {
        if (token == null) {
            throw new ApiException(CodeMsg.NOT_LOGIN_ERROR);
        }
        if (!JWTUtils.validateToken(token, uid)) {
            throw new ApiException(CodeMsg.TOKEN_EXPIRE_ERROR);
        }
    }
}
