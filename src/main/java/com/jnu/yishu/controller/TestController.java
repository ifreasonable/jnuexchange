package com.jnu.yishu.controller;


import com.jnu.yishu.config.UploadConfig;
import com.jnu.yishu.entity.*;
import com.jnu.yishu.repository.*;
import com.jnu.yishu.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Timestamp;
import java.time.Instant;


@RestController
public class TestController {

    @Autowired
    BulletinRepository bulletinRepository;
    @Autowired
    ForSaleBookRepository forSaleBookRepository;
    @Autowired
    WantedBookRepository wantedBookRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    DealRepository dealRepository;

    @GetMapping("/test")
    public String initialize() {
        Bulletin bulletin = new Bulletin();
        bulletin.setContent("test content");
        bulletin.setAnnounceTime(Timestamp.from(Instant.now()));
        bulletin.setExpireTime(Timestamp.from(Instant.now()));
        bulletinRepository.saveAndFlush(bulletin);

        User user = new User();
        user.setUsername("test");
        user.setPassword("123456");
        user = userRepository.saveAndFlush(user);

        User user2 = new User();
        user2.setUsername("test2");
        user2.setPassword("123456");
        user2 = userRepository.saveAndFlush(user2);

        WantedBook wantedBook = new WantedBook();
        wantedBook.setOfferPrice(9.99);
        wantedBook.setTitle("test WantedBook");
        wantedBook.setUid(user.getUid());
        wantedBook = wantedBookRepository.saveAndFlush(wantedBook);

        ForSaleBook forSaleBook = new ForSaleBook();
        forSaleBook.setTitle("test ForSaleBook");
        forSaleBook.setQuotedPrice(9.98);
        forSaleBook.setUid(user.getUid());
        forSaleBook = forSaleBookRepository.saveAndFlush(forSaleBook);

        Deal deal = new Deal();
        deal.setBookId(forSaleBook.getBookId());
        deal.setSellerId(user.getUid());
        deal.setPrice(forSaleBook.getQuotedPrice());
        deal.setBuyerId(user2.getUid());
        deal.setTimestamp(Timestamp.from(Instant.now()));
        dealRepository.saveAndFlush(deal);

        Deal deal2 = new Deal();
        deal2.setBookId(wantedBook.getBookId());
        deal2.setSellerId(user2.getUid());
        deal2.setPrice(wantedBook.getOfferPrice());
        deal2.setBuyerId(user.getUid());
        deal2.setTimestamp(Timestamp.from(Instant.now()));
        dealRepository.saveAndFlush(deal2);

        return "Hello. Initialization finished.";
    }

    @Autowired
    FileService fileService;
    @Autowired
    UploadConfig uploadConfig;

    @PostMapping("/test/file")
    public Object fileUpload(@RequestParam(value = "img", required = false) MultipartFile file, @RequestParam(value = "userId", required = false) Long userId) {
        return fileService.uploadFile(file, userId);
    }

    @DeleteMapping("/test/img/{fileName}")
    public void removeFile(
            @PathVariable("fileName") String fileName) {
//        String filePath = uploadConfig.getSavePath() + fileName;
        fileService.deleteFile("/img/" + fileName);
    }
}
