package com.jnu.yishu.controller;

import com.jnu.yishu.common.ApiException;
import com.jnu.yishu.common.CodeMsg;
import com.jnu.yishu.common.util.JWTUtils;
import com.jnu.yishu.entity.User;
import com.jnu.yishu.service.FileService;
import com.jnu.yishu.service.ForSaleBookService;
import com.jnu.yishu.service.UserService;
import com.jnu.yishu.service.WantedBookService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Api(tags = "用户接口")
@RestController
public class UserController {
    @Autowired
    UserService userService;

    @Autowired
    FileService fileService;

    @Autowired
    ForSaleBookService forSaleBookService;

    @Autowired
    WantedBookService wantedBookService;

    @ApiOperation("获取用户信息")
    @GetMapping("/user/{uid}")
    public User getUserInfo(@RequestHeader(value = JWTUtils.TOKEN, required = false) String token,
                            @PathVariable("uid") Long uid) {
        checkId(uid);
        if (!JWTUtils.validateToken(token, uid)) {
            return userService.findUser(uid).noPrivacy();
        }
        return userService.findUser(uid).noPw();
    }

    @ApiOperation("获取用户想要书籍信息")
    @GetMapping("/user/{uid}/wanted")
    public List getUserWanted(@RequestHeader(value = JWTUtils.TOKEN, required = false) String token,
                              @PathVariable("uid") Long uid) {
        checkId(uid);
        return wantedBookService.getWantedBookByUserId(uid);
    }

    @ApiOperation("获取用户待售书籍信息")
    @GetMapping("/user/{uid}/forsale")
    public List getUserForSale(@RequestHeader(value = JWTUtils.TOKEN, required = false) String token,
                               @PathVariable("uid") Long uid) {
        checkId(uid);
        return forSaleBookService.getForSaleBookByUserId(uid);
    }

    @ApiOperation(value = "修改用户信息",
            notes = "header须有与uid对应的token，忽略用户名、信用状态、管理权限、认证状态、头像路径")
    @PutMapping("/user")
    public User alterUser(@RequestHeader(value = JWTUtils.TOKEN, required = false) String token,
                          @Validated({User.ValidAlterUser.class}) @RequestBody User user) {
        if (!JWTUtils.validateToken(token, user.getUid())) {
            throw new ApiException(CodeMsg.UNAUTHORIZED_OPERATION_ERROR);
        }

        return userService.updateUser(user).noPw();
    }

    @ApiOperation(value = "添加或修改用户头像", notes = "header须有与uid对应的token")
    @PutMapping("/user/avatar")
    public User alterAvatar(@RequestHeader(value = JWTUtils.TOKEN, required = false) String token,
                            @RequestParam("uid") Long uid,
                            @RequestParam("avatar") MultipartFile file) {
        User user = getUserByToken(token);
        if (!user.getUid().equals(uid)) {
            throw new ApiException(CodeMsg.UNAUTHORIZED_OPERATION_ERROR);
        }

        // try to delete old avatar
        String oldPath = user.getAvatar();
        fileService.deleteFile(oldPath);

        user.setAvatar(fileService.uploadFile(file, uid));
        return userService.updateUser(user).noPrivacy();
    }

    @ApiOperation(value = "移除用户", notes = "header中必须要有管理员的token才能删除成功")
    @DeleteMapping("/user/{uid}")
    public void removeUser(@RequestHeader(value = JWTUtils.TOKEN, required = false) String token,
                           @PathVariable("uid") Long uid) {
        User user = getUserByToken(token);
        if (!user.getAdminPrivilege()) {
            throw new ApiException(CodeMsg.UNAUTHORIZED_OPERATION_ERROR);
        }
        userService.deleteUser(uid);
    }

    /**
     * Validate token and get corresponding user
     */
    private User getUserByToken(String token) {
        Long uid = Long.valueOf(JWTUtils.validateToken(token));
        checkId(uid);
        return userService.findUser(uid);
    }

    /**
     * Check if this id exists. If not, throw ApiException.
     */
    private void checkId(Long id) {
        if (!userService.existsById(id)) {
            throw new ApiException(CodeMsg.OBJECT_NOT_FOUND_ERROR);
        }
    }
}
