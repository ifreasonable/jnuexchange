package com.jnu.yishu.controller;

import com.jnu.yishu.common.ApiException;
import com.jnu.yishu.common.CodeMsg;
import com.jnu.yishu.common.util.JWTUtils;
import com.jnu.yishu.entity.Book;
import com.jnu.yishu.entity.ForSaleBook;
import com.jnu.yishu.entity.WantedBook;
import com.jnu.yishu.service.WantedBookService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;

// TODO entity specific operation

@Api(tags = {"想要书籍接口"})
@RestController
public class WantedBookController {
    @Autowired
    WantedBookService wantedBookService;

    @ApiOperation("获取想要书籍列表（有分页）")
    @GetMapping("/wantedBook")
    public List getWantedBooks(
            @ApiParam(defaultValue = "1") @PathParam("currentPage") Integer currentPage,
            @ApiParam(defaultValue = "10") @PathParam("pageSize") Integer pageSize,
            @ApiParam(value = "按该属性排序。可以传多个。") @PathParam("prop") ArrayList<String> prop,
            @ApiParam(value = "排序方式。（0为降序，1为升序）可以传多个。\n如果列表为空时加入该参数会收到bad request") @PathParam("sort") ArrayList<Integer> sort) {
        Pageable pageable;
        ArrayList<Sort.Order> orders = new ArrayList<>();

        if (currentPage == null) currentPage = 1;
        if (pageSize == null) pageSize = 10;

        if (prop.size() == 0) {
            pageable = PageRequest.of(currentPage, pageSize);
        } else {
            for (int i = 0; i < prop.size(); i += 1) {
                if (sort.size() > i && sort.get(i) == 1)
                    orders.add(new Sort.Order(Sort.Direction.ASC, prop.get(i)));
                else
                    orders.add(new Sort.Order(Sort.Direction.DESC, prop.get(i)));
            }
            pageable = PageRequest.of(currentPage, pageSize, Sort.by(orders));
        }

        return wantedBookService.getAllWantedBook(pageable).getContent();
    }

    @ApiOperation("获取单个想要书籍")
    @GetMapping("/wantedBook/{id}")
    public WantedBook getWantedBookDetail(@PathVariable("id") Long id) {
        checkId(id);
        return wantedBookService.getWantedBook(id);
    }

    @ApiOperation("获取想要书籍总数量")
    @GetMapping("/wantedBook/count")
    public Long getWantedBookCount() {
        return wantedBookService.getWantedBookCount();
    }

    @ApiOperation(value = "新建想要书籍", notes = "json可以不包含该实体的id，即使包含，也将被忽略")
    @PostMapping("/wantedBook")
    public WantedBook newWantedBook(
            @RequestHeader(value = JWTUtils.TOKEN, required = false) String token,
            @Validated({Book.ValidNewBook.class}) @RequestBody WantedBook wantedBook) {
        checkUser(token);
        //前端不能维护id,所以不如直接忽略。而且jpa要求为null才认为是新的。
        wantedBook.setBookId(null);
        checkOwner(token, wantedBook.getUid());
        return wantedBookService.insertWantedBook(wantedBook);
    }

    @ApiOperation("修改想要书籍")
    @PutMapping("/wantedBook")
    public WantedBook alterWantedBook(
            @RequestHeader(value = JWTUtils.TOKEN, required = false) String token,
            @Validated({Book.ValidAlterBook.class}) @RequestBody WantedBook wantedBook) {
        checkUser(token);
        checkOwner(token, wantedBook.getUid());
        return wantedBookService.updateWantedBook(wantedBook);
    }

    @ApiOperation("删除想要书籍")
    @DeleteMapping("/wantedBook/{id}")
    public void removeWantedBook(
            @RequestHeader(value = JWTUtils.TOKEN, required = false) String token,
            @PathVariable("id") Long id) {
        checkUser(token);
        checkId(id);
        checkOwner(token, wantedBookService.getWantedBook(id).getUid());
        wantedBookService.deleteWantedBook(id);
    }

    @ApiOperation("搜索想要书籍")
    @GetMapping("/wantedBook/search")
    public List<WantedBook> searchForSaleBook(
            @RequestHeader(value = JWTUtils.TOKEN, required = false) String token,
            @PathParam("keyword") String keyword) {
        checkUser(token);
        if(keyword == null || keyword.equals("")) return new ArrayList<>();

        keyword = keyword.replace(" ", "");
        return wantedBookService.searchForSaleBook(keyword);
    }

    /**
     * Check if token exists. If not, throw ApiException.
     * If yes, check if it is valid. If not valid, throw ApiException.
     */
    private void checkUser(String token) {
        if (token == null) throw new ApiException(CodeMsg.NOT_LOGIN_ERROR);
        if (JWTUtils.validateToken(token).equals(""))
            throw new ApiException(CodeMsg.TOKEN_EXPIRE_ERROR);
    }

    /**
     * Check if uid column of entity corresponds to token.
     * If not valid, throw ApiException.
     *
     * @param token user token in header
     * @param uid   uid extracted from entity
     */
    private void checkOwner(String token, Long uid) {
        if (!JWTUtils.validateToken(token, uid))
            throw new ApiException(CodeMsg.UNAUTHORIZED_OPERATION_ERROR);
    }

    /**
     * Check if this id exists. If not, throw ApiException.
     */
    private void checkId(Long id) {
        if (!wantedBookService.isWantedBookExists(id))
            throw new ApiException(CodeMsg.OBJECT_NOT_FOUND_ERROR);
    }
}
