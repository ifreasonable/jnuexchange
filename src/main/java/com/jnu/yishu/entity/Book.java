package com.jnu.yishu.entity;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Book {
    @ApiModelProperty("bookId")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "book_id", nullable = false)
    @NotNull(groups = {ValidAlterBook.class}, message = "bookId不能为空！")
    private Long bookId;

    @ApiModelProperty(value = "卖家/需求者 uid", required = true)
    @NotNull(groups = {ValidNewBook.class, ValidAlterBook.class}, message = "uid不能为空！")
    private Long uid;

    @ApiModelProperty(value = "书名", required = true)
    @NotNull(groups = {ValidNewBook.class}, message = "书名不能为空！")
    private String title;

    @ApiModelProperty("图片")
    private String pic;

    @ApiModelProperty("专业代码")
    private String major;

    @ApiModelProperty("是否必修")
    private Boolean compulsory;

    @ApiModelProperty("简介")
    private String introduction;

    public interface ValidNewBook {
    }

    public interface ValidAlterBook {
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bId) {
        this.bookId = bId;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uId) {
        this.uid = uId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public Boolean getCompulsory() {
        return compulsory;
    }

    public void setCompulsory(Boolean compulsory) {
        this.compulsory = compulsory;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String remark) {
        this.introduction = remark;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
