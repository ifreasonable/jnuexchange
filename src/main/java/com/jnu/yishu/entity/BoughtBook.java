package com.jnu.yishu.entity;


import io.swagger.annotations.ApiModelProperty;

@Deprecated
//@Entity
public class BoughtBook extends Book {
    @ApiModelProperty(value = "交易id", required = true)
    private Long dealId;

    public Long getDealId() {
        return dealId;
    }

    public void setDealId(Long dealId) {
        this.dealId = dealId;
    }
}
