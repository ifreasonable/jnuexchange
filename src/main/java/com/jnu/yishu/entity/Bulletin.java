package com.jnu.yishu.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.jnu.yishu.common.CodeMsg;
import com.jnu.yishu.common.util.validator.AtLeastOneNotEmpty;
import com.jnu.yishu.entity.Bulletin.ValidAlterBulletin;
import com.jnu.yishu.entity.Bulletin.ValidNewBulletin;
import io.swagger.annotations.ApiModelProperty;

import java.sql.Timestamp;

@Entity
@AtLeastOneNotEmpty(fields = {"announceTime", "expireTime"},
        groups = {ValidNewBulletin.class, ValidAlterBulletin.class},
        message = "提交的时间戳必须构成非空集合")
public class Bulletin {
    @ApiModelProperty(value = "公告id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bulletin_id", nullable = false)
    @NotNull(groups = {ValidAlterBulletin.class},
            message = "未传入对象id")
    private Long bulletinId;                // 公告id

    @ApiModelProperty(value = "公告内容", required = true)
    @NotNull(groups = {ValidNewBulletin.class, ValidAlterBulletin.class},
            message = "内容不能为空")
    private String content;

    @ApiModelProperty("发布时间")
    private Timestamp announceTime;

    @ApiModelProperty("到期时间")
    private Timestamp expireTime;

    @ApiModelProperty("筛选用户id")
    private Long filterUid;

    @ApiModelProperty("筛选用户专业")
    private Integer filterMajor;

    @ApiModelProperty("筛选用户信用状态")
    private Integer filterCreditStatus;

    public interface ValidNewBulletin {
    }

    public interface ValidAlterBulletin {
    }

    public Long getBulletinId() {
        return bulletinId;
    }

    public void setBulletinId(Long bulletinId) {
        this.bulletinId = bulletinId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Timestamp expireTime) {
        this.expireTime = expireTime;
    }

    public Long getFilterUid() {
        return filterUid;
    }

    public void setFilterUid(Long filterUid) {
        this.filterUid = filterUid;
    }

    public Integer getFilterMajor() {
        return filterMajor;
    }

    public void setFilterMajor(Integer filterMajor) {
        this.filterMajor = filterMajor;
    }

    public Integer getFilterCreditStatus() {
        return filterCreditStatus;
    }

    public void setFilterCreditStatus(Integer filterCreditStatus) {
        this.filterCreditStatus = filterCreditStatus;
    }

    public Timestamp getAnnounceTime() {
        return announceTime;
    }

    public void setAnnounceTime(Timestamp announceTime) {
        this.announceTime = announceTime;
    }
}
