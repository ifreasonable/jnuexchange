package com.jnu.yishu.entity;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Entity
public class Deal {
    @ApiModelProperty("dealId")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "deal_id", nullable = false)
    @NotNull(groups = {ValidAlterDeal.class}, message = "dealId不能为空！")
    private Long dealId;

    @NotNull(groups = {ValidNewDeal.class, ValidAlterDeal.class}, message = "卖家uid不能为空！")
    @ApiModelProperty(value = "卖家uid", required = true)
    private Long sellerId;

    @NotNull(groups = {ValidNewDeal.class, ValidAlterDeal.class}, message = "买家uid不能为空！")
    @ApiModelProperty(value = "买家uid", required = true)
    private Long buyerId;

    @ApiModelProperty(value = "交易时间", required = false)
    private Timestamp timestamp;

    @ApiModelProperty(value = "交易书籍id", required = true)
    @NotNull(groups = {ValidNewDeal.class, ValidAlterDeal.class}, message = "书籍id不能为空！")
    private Long bookId;

    @ApiModelProperty(value = "成交价格", required = true)
    @NotNull(groups = {ValidNewDeal.class, ValidAlterDeal.class}, message = "成交价格不能为空！")
    private Double price;

    private Integer status;         // TODO：状态码（未定义）

    public interface ValidNewDeal {
    }

    public interface ValidAlterDeal {
    }

    public Long getDealId() {
        return dealId;
    }

    public void setDealId(Long dealId) {
        this.dealId = dealId;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(Long buyerId) {
        this.buyerId = buyerId;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bId) {
        this.bookId = bId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
