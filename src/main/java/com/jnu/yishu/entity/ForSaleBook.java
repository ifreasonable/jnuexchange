package com.jnu.yishu.entity;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


@Entity
public class ForSaleBook extends Book {
    @ApiModelProperty(value = "期望售价", required = true)
    @Min(groups = {ValidNewBook.class, ValidAlterBook.class}, value = 0)
    @NotNull(groups = {ValidNewBook.class, ValidAlterBook.class}, message = "售价不能为空！")
    private Double quotedPrice;

    private Integer status;             // TODO：状态码（未定义）

    public double getQuotedPrice() {
        return quotedPrice;
    }

    public void setQuotedPrice(double quotedPrice) {
        this.quotedPrice = quotedPrice;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
