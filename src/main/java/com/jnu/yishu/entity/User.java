package com.jnu.yishu.entity;

import com.jnu.yishu.common.util.BeanCopyUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.*;

@ApiModel
@Entity
public class User {
    @ApiModelProperty("uid")
    @Id
    @Column(name = "uid", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long uid;                   // 用户id

    @ApiModelProperty(value = "密码",required = true)
    @NotNull(groups = {ValidSignup.class, ValidLogin.class}, message = "密码不能为空")
    @NotBlank(groups = {ValidSignup.class, ValidLogin.class}, message = "密码不能为空")
    @Pattern(groups = {ValidSignup.class, ValidAlterUser.class},
            message = "密码仅能包含字母、数字或$@!%*#?&，长度6-18位",
            regexp = "^[A-Za-z0-9$@!%*#?&]{6,18}$")
    private String password;            // 密码

    @ApiModelProperty(value = "用户注册名",required = true)
    @NotNull(groups = {ValidSignup.class, ValidLogin.class}, message = "用户名不能为空")
    @NotBlank(groups = {ValidSignup.class, ValidLogin.class}, message = "用户名不能为空")
    @Length(groups = {ValidSignup.class, ValidAlterUser.class}, max = 18, message = "用户名不能多于18个字符")
    @Pattern(groups = {ValidSignup.class, ValidAlterUser.class},
            message = "用户名仅能包含字母、数字、汉字或下划线",
            regexp = "^[A-Za-z0-9_\\u4E00-\\u9FA5]+$")
    private String username;            // 用户注册名

    @ApiModelProperty("是否为管理员")
    private Boolean adminPrivilege = false;         // 管理员权限

    @ApiModelProperty("信用状态")
    private Integer creditStatus = 100;             // 信用状态
    @ApiModelProperty("实名认证状态")
    private Boolean authenticated = false;        // 实名认证状态

    @ApiModelProperty("昵称")
    @Length(groups = {ValidSignup.class, ValidAlterUser.class}, max = 18, message = "昵称不能多于18个字符")
    @Pattern(groups = {ValidSignup.class, ValidAlterUser.class},
            message = "昵称仅能包含字母、数字、汉字或_$@!%*#?&",
            regexp = "^[A-Za-z0-9_\\u4E00-\\u9FA5$@!%*#?&]+$")
    private String nickname;            // 昵称
    @ApiModelProperty("头像路径")
    private String avatar;              // 头像

    @ApiModelProperty("学号")
    private Long studentId;             // 学号
    @ApiModelProperty("校区")
    private String campus;
    @ApiModelProperty("学院")
    private String school;
    @ApiModelProperty("专业代码")
    private String major;


    // 联系方式
    @ApiModelProperty("电话号码")
    private Long phone;                 // 手机号
//    @Pattern(groups = {ValidSignup.class, ValidAlter.class},
//            message = "微信号仅能包含字母、数字或下划线，长度6-20位",
//            regexp = "^[A-Za-z0-9_]{6,20}$")
    @ApiModelProperty("微信号")
    private String wechat;              // 微信号
    @ApiModelProperty("QQ")
    private Long qq;                    // qq
    @ApiModelProperty("邮箱地址")
    @Email(message = "错误的邮箱格式", groups = {ValidSignup.class, ValidAlterUser.class})
    private String email;               // 邮箱地址

    // 隐私设置
    @ApiModelProperty("是否公开学号")
    private Boolean visibleStudentId = true;   // 是否公开学号
    @ApiModelProperty("是否公开联系方式")
    private Boolean visibleContact = true;     // 是否公开联系方式
    @ApiModelProperty("是否公开校区")
    private Boolean visibleCampus = true;      // 是否公开校区
    @ApiModelProperty("是否公开学院")
    private Boolean visibleSchool = true;      // 是否公开学院
    @ApiModelProperty("是否公开专业")
    private Boolean visibleMajor = true;       // 是否公开专业

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public interface ValidLogin {
    }

    public interface ValidSignup {
    }

    public interface ValidAlterUser {
    }

    public User noPw() {
        this.setPassword(null);
        return this;
    }

    public User noPrivacy() {
        this.setPassword(null);
        if(!visibleStudentId) this.setStudentId(null);
        if(!visibleContact) {
            this.setPhone(null);
            this.setEmail(null);
            this.setQq(null);
            this.setWechat(null);
        }
        if(!visibleMajor) this.setMajor(null);
        if(!visibleSchool) this.setSchool(null);
        if(!visibleCampus) this.setCampus(null);
        return this;
    }

    public void copyFrom(User user, String ... ignore) {
        BeanCopyUtils.beanCopyWithIgnore(user, this, ignore);
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getAdminPrivilege() {
        return adminPrivilege;
    }

    public void setAdminPrivilege(Boolean adminPrivilege) {
        this.adminPrivilege = adminPrivilege;
    }

    public Integer getCreditStatus() {
        return creditStatus;
    }

    public void setCreditStatus(Integer creditStatus) {
        this.creditStatus = creditStatus;
    }

    public Boolean getAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(Boolean authenticated) {
        this.authenticated = authenticated;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public Long getQq() {
        return qq;
    }

    public void setQq(Long qq) {
        this.qq = qq;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getVisibleStudentId() {
        return visibleStudentId;
    }

    public void setVisibleStudentId(Boolean studentIdVisible) {
        this.visibleStudentId = studentIdVisible;
    }

    public Boolean getVisibleContact() {
        return visibleContact;
    }

    public void setVisibleContact(Boolean contactVisible) {
        this.visibleContact = contactVisible;
    }

    public Boolean getVisibleCampus() {
        return visibleCampus;
    }

    public void setVisibleCampus(Boolean campusVisible) {
        this.visibleCampus = campusVisible;
    }

    public Boolean getVisibleSchool() {
        return visibleSchool;
    }

    public void setVisibleSchool(Boolean schoolVisible) {
        this.visibleSchool = schoolVisible;
    }

    public Boolean getVisibleMajor() {
        return visibleMajor;
    }

    public void setVisibleMajor(Boolean majorVisible) {
        this.visibleMajor = majorVisible;
    }
}
