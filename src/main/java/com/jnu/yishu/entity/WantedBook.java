package com.jnu.yishu.entity;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
public class WantedBook extends Book {
    @ApiModelProperty(value = "报价", required = true)
    @Min(groups = {ValidNewBook.class, ValidAlterBook.class}, value = 0)
    @NotNull(groups = {ValidNewBook.class, ValidAlterBook.class}, message = "报价不能为空！")
    private Double offerPrice;
    private Integer status;         // TODO：状态码（未定义）

    public Double getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(Double offerPrice) {
        this.offerPrice = offerPrice;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
