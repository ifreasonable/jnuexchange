package com.jnu.yishu.repository;

import java.sql.Timestamp;
import java.util.List;

import com.jnu.yishu.entity.Bulletin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BulletinRepository extends JpaRepository<Bulletin, Long> {
    public List<Bulletin> findByAnnounceTimeBeforeAndExpireTimeAfter(Timestamp timestamp1, Timestamp timestamp2);

    public Long countByAnnounceTimeBeforeAndExpireTimeAfter(Timestamp timestamp1, Timestamp timestamp2);
}