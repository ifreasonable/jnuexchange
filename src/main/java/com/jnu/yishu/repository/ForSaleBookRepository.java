package com.jnu.yishu.repository;

import com.jnu.yishu.entity.ForSaleBook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ForSaleBookRepository extends JpaRepository<ForSaleBook, Long> {
    List<ForSaleBook> findByUid(Long uid);

//    @Query("select b from ForSaleBook b where b.title like ?1 OR b.major like ?1 or b.introduction like ?1")
//    List<ForSaleBook> searchByKeyword(String keyword);

    @Query("select b from ForSaleBook b where CONCAT(b.title, b.introduction, b.major) like ?1" +
            "order by (case when b.title like ?1 then 1 else 0 end) + " +
            "(case when b.introduction like ?1 then 1 else 0 end) + " +
            "(case when b.major like ?1 then 1 else 0 end) DESC")
    List<ForSaleBook> searchByKeyword(String keyword);
}
