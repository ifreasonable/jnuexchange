package com.jnu.yishu.repository;

import com.jnu.yishu.entity.ForSaleBook;
import com.jnu.yishu.entity.WantedBook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WantedBookRepository extends JpaRepository<WantedBook, Long> {
    List<WantedBook> findByUid(Long uid);

    @Query("select b from WantedBook b where CONCAT(b.title, b.introduction, b.major) like ?1" +
            "order by (case when b.title like ?1 then 1 else 0 end) + " +
            "(case when b.introduction like ?1 then 1 else 0 end) + " +
            "(case when b.major like ?1 then 1 else 0 end) DESC")
    List<WantedBook> searchByKeyword(String keyword);
}