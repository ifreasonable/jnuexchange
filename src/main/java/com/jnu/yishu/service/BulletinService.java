package com.jnu.yishu.service;

import com.jnu.yishu.entity.Bulletin;
import com.jnu.yishu.repository.BulletinRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Service
public class BulletinService {
    @Autowired
    BulletinRepository bulletinRepository;

    public Page getAllBulletin(Pageable pageable) {
        return bulletinRepository.findAll(pageable);
    }

    public long getBulletinCount() {
        return bulletinRepository.count();
    }

    public Bulletin getBulletin(Long id) {
        Optional<Bulletin> result = bulletinRepository.findById(id);
        return result.orElseGet(Bulletin::new);
    }

    public Boolean isBulletinExists(Long id) {
        return bulletinRepository.existsById(id);
    }

    public Bulletin insertBulletin(Bulletin bulletin) {
        return bulletinRepository.saveAndFlush(bulletin);
    }

    public Bulletin updateBulletin(Bulletin bulletin) {
        return bulletinRepository.save(bulletin);
    }

    public void deleteBulletin(Long id) {
        bulletinRepository.deleteById(id);
    }

    //TODO 测试
    public Long getAliveBulletinCount(Timestamp timestamp) {
        return bulletinRepository.countByAnnounceTimeBeforeAndExpireTimeAfter(timestamp, timestamp);
    }

    public List<Bulletin> getAliveBulletin(Timestamp timestamp) {
        return bulletinRepository.findByAnnounceTimeBeforeAndExpireTimeAfter(timestamp, timestamp);
    }

}
