package com.jnu.yishu.service;

import com.jnu.yishu.entity.Deal;
import com.jnu.yishu.repository.DealRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DealService {
    @Autowired
    DealRepository dealRepository;

    public Page getAllDeal(Pageable pageable) {
        return dealRepository.findAll(pageable);
    }

    public long getDealCount() {
        return dealRepository.count();
    }

    public Deal getDeal(Long id) {
        Optional<Deal> result = dealRepository.findById(id);
        return result.orElseGet(Deal::new);
    }

    public Deal insertDeal(Deal deal) {
        return dealRepository.saveAndFlush(deal);
    }

    public Deal updateDeal(Deal deal) {
        return dealRepository.save(deal);
    }

    public void deleteDeal(Long id) {
        dealRepository.deleteById(id);
    }

    public boolean isDealExists(Long bookId) {
        return dealRepository.existsById(bookId);
    }
}

