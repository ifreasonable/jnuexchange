package com.jnu.yishu.service;

import com.jnu.yishu.common.ApiException;
import com.jnu.yishu.common.CodeMsg;
import com.jnu.yishu.common.util.FileUtils;
import com.jnu.yishu.config.UploadConfig;
import com.jnu.yishu.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class FileService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    UploadConfig uploadConfig;

    Logger logger = Logger.getLogger(FileService.class.getName());


    public String uploadFile(MultipartFile file, Long userId) {
        if (file.isEmpty()) {
            throw new ApiException(CodeMsg.CONTENT_IS_EMPTY_ERROR);
        }
        if(!userRepository.existsById(userId)){
            throw new ApiException(CodeMsg.USER_NOT_EXIST);
        }

        String fileName = file.getOriginalFilename();
        if(fileName == null) {
            fileName = "";
        }
        fileName = FileUtils.renameToUUID(fileName);

        try {
            FileUtils.saveFile(file.getBytes(), uploadConfig.getSavePath(), fileName);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "failed to save file", e);
            throw new ApiException(CodeMsg.UPLOAD_FAILED_ERROR);
        }

        logger.log(Level.INFO, "user " + userId + " upload file " + fileName);
        return uploadConfig.getRequestPath() + fileName;
    }

    /**
     * 删除文件
     * @param filePath 文件的访问路径
     */
    public void deleteFile(String filePath) {
        if (filePath == null) return;

        // get path in server
        String fileName = Paths.get(filePath).getFileName().toString();
        filePath = uploadConfig.getSavePath() + fileName;

        if(!FileUtils.deleteFile(filePath)) {
            throw new ApiException(CodeMsg.OBJECT_NOT_FOUND_ERROR);
        }
    }
}
