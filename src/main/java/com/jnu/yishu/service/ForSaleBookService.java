package com.jnu.yishu.service;

import com.jnu.yishu.entity.ForSaleBook;
import com.jnu.yishu.repository.ForSaleBookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ForSaleBookService {
    @Autowired
    ForSaleBookRepository forSaleBookRepository;

    public Page getAllForSaleBook(Pageable pageable) {
        return forSaleBookRepository.findAll(pageable);
    }

    public long getForSaleBookCount() {
        return forSaleBookRepository.count();
    }

    public ForSaleBook getForSaleBook(Long id) {
        Optional<ForSaleBook> result = forSaleBookRepository.findById(id);
        return result.orElseGet(ForSaleBook::new);
    }

    public ForSaleBook insertForSaleBook(ForSaleBook forSaleBook) {
        return forSaleBookRepository.saveAndFlush(forSaleBook);
    }

    public ForSaleBook updateForSaleBook(ForSaleBook forSaleBook) {
        return forSaleBookRepository.save(forSaleBook);
    }

    public void deleteForSaleBook(Long id) {
        forSaleBookRepository.deleteById(id);
    }

    public boolean isForSaleBookExists(Long bookId) {
        return forSaleBookRepository.existsById(bookId);
    }

    public List getForSaleBookByUserId(Long uid) {
        return forSaleBookRepository.findByUid(uid);
    }

    public List<ForSaleBook> searchForSaleBook(String keyword) {
        return forSaleBookRepository.searchByKeyword("%" + keyword + "%");
    }
}
