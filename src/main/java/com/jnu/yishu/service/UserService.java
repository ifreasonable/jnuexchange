package com.jnu.yishu.service;

import com.jnu.yishu.common.ApiException;
import com.jnu.yishu.common.CodeMsg;
import com.jnu.yishu.common.util.JWTUtils;
import com.jnu.yishu.entity.User;
import com.jnu.yishu.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public User signup(User user) {
        // 用户已存在时
        if (userRepository.existsByUsername(user.getUsername())) {
            throw new ApiException(CodeMsg.OCCUPIED_USERNAME_ERROR);
        }
        User signupInfo = new User();
        signupInfo.copyFrom(user, "uid", "adminPrivilege", "authenticated", "avatar", "creditStatus");
        return userRepository.saveAndFlush(signupInfo);
    }

    public String login(User user) {
        // 用户不存在时
        if (!userRepository.existsByUsername(user.getUsername())) {
            throw new ApiException(CodeMsg.USER_NOT_EXIST);
        }

        User rUser = findByUsername(user.getUsername());
        if (!Objects.equals(user.getPassword(), rUser.getPassword())) {
            throw new ApiException(CodeMsg.LOGIN_PASSWORD_ERROR);
        } else {
            return JWTUtils.createToken(String.valueOf(rUser.getUid()));
        }
    }

    public Boolean existsById(Long id) {
        return userRepository.existsById(id);
    }

    public User findUser(Long id) {
        Optional<User> result = userRepository.findById(id);
        return result.orElseThrow(() -> new ApiException(CodeMsg.OBJECT_NOT_FOUND_ERROR));
    }

    public User findByUsername(String username) {
        Optional<User> result = userRepository.findByUsername(username);
        return result.orElseThrow(() -> new ApiException(CodeMsg.OBJECT_NOT_FOUND_ERROR));
    }

    public User updateUser(User user) {
        User saveUser = findUser(user.getUid());
        saveUser.copyFrom(user, "username", "adminPrivilege", "authenticated", "avatar", "creditStatus");
        return userRepository.save(saveUser);
    }

    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }
}
