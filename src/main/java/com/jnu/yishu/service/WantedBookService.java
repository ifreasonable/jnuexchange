package com.jnu.yishu.service;

import com.jnu.yishu.entity.ForSaleBook;
import com.jnu.yishu.entity.WantedBook;
import com.jnu.yishu.repository.WantedBookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class WantedBookService {
    @Autowired
    WantedBookRepository wantedBookRepository;

    public Page getAllWantedBook(Pageable pageable) {
        return wantedBookRepository.findAll(pageable);
    }

    public long getWantedBookCount() {
        return wantedBookRepository.count();
    }

    public WantedBook getWantedBook(Long id) {
        Optional<WantedBook> result = wantedBookRepository.findById(id);
        return result.orElseGet(WantedBook::new);
    }

    public WantedBook insertWantedBook(WantedBook wantedBook) {
        return wantedBookRepository.saveAndFlush(wantedBook);
    }

    public WantedBook updateWantedBook(WantedBook wantedBook) {
        return wantedBookRepository.save(wantedBook);
    }

    public void deleteWantedBook(Long id) {
        wantedBookRepository.deleteById(id);
    }

    public boolean isWantedBookExists(Long bookId) {
        return wantedBookRepository.existsById(bookId);
    }

    public List getWantedBookByUserId(Long uid) {
        return wantedBookRepository.findByUid(uid);
    }

    public List<WantedBook> searchForSaleBook(String keyword) {
        return wantedBookRepository.searchByKeyword("%" + keyword + "%");
    }
}
