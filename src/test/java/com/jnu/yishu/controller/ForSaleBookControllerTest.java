package com.jnu.yishu.controller;

import com.alibaba.fastjson.JSONObject;
import org.apache.catalina.connector.Response;
import org.junit.jupiter.api.Test;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class ForSaleBookControllerTest {

    public static String HttpRestClient(String url, HttpMethod method, JSONObject json) throws IOException {
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setConnectTimeout(10*1000);
        requestFactory.setReadTimeout(10*1000);
        RestTemplate client = new RestTemplate(requestFactory);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PROBLEM_JSON_UTF8);
        HttpEntity<String> requestEntity = new HttpEntity<String>(json.toString(), headers);
        //  执行HTTP请求
        ResponseEntity<String> response = client.exchange(url, method, requestEntity, String.class);
        return response.getBody();
    }

    @Test
    void newForSaleBook() {
        //api url地址
        String url1 = "http://localhost:8080/login";
        String url2 = "http://localhost:8080/forSaleBook";
        try{
                //用户登录
                HttpMethod method =HttpMethod.POST;
                JSONObject json = new JSONObject();
                json.put("username", "pengzhihui");
                json.put("password", "123456");
                String result1 = ForSaleBookControllerTest.HttpRestClient(url1,method,json);

//                //设置forsalebook
//                HttpMethod method2 =HttpMethod.POST;
//                JSONObject json2 = new JSONObject();
//                json.put("uid", 1);
//                json.put("title", "软件工程");
//                json.put("quotedPrice", 123.3);
//                //System.out.print("发送数据："+json.toString());
//                //发送http请求并返回结果
//                String result2 = ForSaleBookControllerTest.HttpRestClient(url2,method,json);
//                //System.out.print("接收反馈："+result);

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}