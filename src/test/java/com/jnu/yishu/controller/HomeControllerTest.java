package com.jnu.yishu.controller;

import org.junit.jupiter.api.Test;
import com.alibaba.fastjson.JSONObject;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import java.io.IOException;
import java.util.HashSet;
import java.util.Random;
import org.springframework.test.annotation.Rollback;
import javax.transaction.Transactional;

class HomeControllerTest {

    public static String HttpRestClient(String url, HttpMethod method, JSONObject json) throws IOException {
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setConnectTimeout(10*1000);
        requestFactory.setReadTimeout(10*1000);
        RestTemplate client = new RestTemplate(requestFactory);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PROBLEM_JSON_UTF8);
        HttpEntity<String> requestEntity = new HttpEntity<String>(json.toString(), headers);
        //  执行HTTP请求
        ResponseEntity<String> response = client.exchange(url, method, requestEntity, String.class);
        return response.getBody();
    }

    //生成随机的不重复的字符串  length为长度
    public static String[] getRandomStringArray(int length,int size) {
        String[] strs = new String[size];
        char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        HashSet<String> set = new HashSet<>();
        while (set.size() < size) {//生成随机字符串到set里面
            sb.setLength(0);
            for (int i = 0; i < length; i++)
                sb.append(chars[random.nextInt(62)]);
            set.add(sb.toString());
        }
        int i = 0;
        for (String s : set)//将set里面的数据存放到数组
            strs[i++] = s;
        return strs;
    }
    //这里主要是测试密码长度
    @Transactional
    @Rollback()
    @Test
    //注册测试
    void signupTest() {
        //api url地址
        String url = "http://localhost:8080/signup";
        String[] username=HomeControllerTest.getRandomStringArray(10,10000);
        String[] password={"1","12","123","1234","12345","123456","1234567","12345678","123456789","1234567890"};
        String largepassword="我喜欢软件工程";
        int count=1;
        try{
//                for(int i=5;i>=0;--i){
//                    //post请求
//                    HttpMethod method =HttpMethod.POST;
//                    JSONObject json = new JSONObject();
//                    json.put("username", username[i]);
//                    json.put("password", password[i]);
//                    //System.out.print("发送数据："+json.toString());
//                    //发送http请求并返回结果
//                    String result = HomeControllerTest.HttpRestClient(url,method,json);
//                    System.out.print("接收反馈："+result);
//                    //用count标识通过的数据
//                    System.out.println(count++);
//                }
                HttpMethod method =HttpMethod.POST;
                JSONObject json = new JSONObject();
                json.put("username", username[100]);
                json.put("password", largepassword);
                //System.out.print("发送数据："+json.toString());
                //发送http请求并返回结果
                String result = HomeControllerTest.HttpRestClient(url,method,json);
                System.out.print("接收反馈："+result);
                //用count标识通过的数据
                System.out.println(count++);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}