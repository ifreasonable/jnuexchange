package com.jnu.yishu.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jnu.yishu.entity.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

//对UserControllerce层进行测试
@SpringBootTest
class UserControllerTest {
    private MockMvc mvc;
    @Autowired
    private WebApplicationContext wac;

    @Transactional
    @Rollback()
    @Test
    void getUserInfo() {
        //创建一个模拟对象
        mvc = MockMvcBuilders.webAppContextSetup(wac).build();
        // 发送get请求，去TestController预设的initialize初始化数据先
        RequestBuilder request = get("http://localhost:8080/test");

        // 发送get请求,获取uid为2的用户信息
        RequestBuilder request2 = get("http://localhost:8080/user/2");
        try {
            String response = mvc.perform(request2).andReturn().getResponse().getContentAsString();
            System.out.println(response);
            //jackson解析
            ObjectMapper mapper = new ObjectMapper();
            User user = mapper.readValue(response, User.class);
            System.out.println(user);
            Assertions.assertEquals(user.getUid(), 2);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //发送post请求：
        //RequestBuilder request = post("").content("");
        //mvc.perform(request);
    }
}