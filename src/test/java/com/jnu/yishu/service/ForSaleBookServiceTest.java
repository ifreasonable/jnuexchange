package com.jnu.yishu.service;

import com.jnu.yishu.entity.ForSaleBook;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import javax.annotation.Resource;
import javax.transaction.Transactional;

@Deprecated
@SpringBootTest
class ForSaleBookServiceTest {
    long bookid = 1L;
    long uid = 112233L;
    String title="三国演义";
    double quotedPrice=32.5;
    @Resource
    ForSaleBookService forSaleBookService;

    @Transactional
    @Rollback()
    @Test
    void insertForSaleBook() {
        //create a testing forSaleBook
        ForSaleBook forSaleBook = new ForSaleBook();
        forSaleBook.setQuotedPrice(quotedPrice);
        forSaleBook.setBookId(bookid);
        forSaleBook.setTitle(title);
        forSaleBook.setUid(uid);
        forSaleBookService.insertForSaleBook(forSaleBook);


        ForSaleBook resultbook = forSaleBookService.getForSaleBook(bookid);
        Assertions.assertEquals(resultbook.getTitle(),title);
    }
}