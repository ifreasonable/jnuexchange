package com.jnu.yishu.service;
//UserService层测试完成
import com.jnu.yishu.common.util.JWTUtils;
import com.jnu.yishu.entity.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.*;
//对UserService层进行测试
@SpringBootTest
class UserServiceTest {
    long id;
    String username="pengzhihui";
    String password="pzh12345";
    @Resource
    UserService userService;

    @BeforeEach
    void setUp() {

    }

    @AfterEach
    void tearDown() {

    }

    @Transactional
    @Rollback()
    @Test
    //测试注册用户数据是否存储正确，是否能从数据库中读取数据
    void signup() {
        //create a testing user
        User myu=new User();
        myu.setUsername(username);
        myu.setPassword(password);
        userService.signup(myu);

        //testing
        User user1=userService.findByUsername(username);
        id=user1.getUid();
        assertEquals(user1.getUsername(),username);
    }

    //测试查询是否存在的功能
    @Transactional
    @Rollback()
    @Test
    void existsById() {
        //create a testing user
        User myu=new User();
        myu.setUsername(username);
        myu.setPassword(password);
        userService.signup(myu);
        //testing
        User user1=userService.findByUsername(username);
        id=user1.getUid();
        Assertions.assertEquals(true,userService.existsById(id));
    }

    //测试是否删除用户成功
    @Transactional
    @Rollback()
    @Test
    void deleteUser() {
        //create a testing user
        User myu=new User();
        myu.setUsername(username);
        myu.setPassword(password);
        userService.signup(myu);
        //testing
        User user1=userService.findByUsername(username);
        id=user1.getUid();
        Assertions.assertEquals(user1.getUsername(),username);
        Assertions.assertEquals(true,userService.existsById(id));
        userService.deleteUser(id);
        Assertions.assertEquals(false,userService.existsById(id));
    }

    //测试用户是否可以登录成功
    @Transactional
    @Rollback()
    @Test
    void login() {
        //create a testing user
        User myu=new User();
        myu.setUsername(username);
        myu.setPassword(password);
        userService.signup(myu);
        //testing
        User user1=userService.findByUsername(username);
        id=user1.getUid();
        String right=JWTUtils.createToken(String.valueOf(id));
        Assertions.assertEquals(right,userService.login(myu));
    }
    //测试是否可以从getUser中通过id号拿到对应的user
    @Transactional
    @Rollback()
    @Test
    void getUser() {
        //create a testing user
        User myu=new User();
        myu.setUsername(username);
        myu.setPassword(password);
        userService.signup(myu);
        User user1=userService.findByUsername(username);
        id=user1.getUid();
        //testing
        assertEquals(myu.getUsername(),userService.findUser(id).getUsername());
    }
}